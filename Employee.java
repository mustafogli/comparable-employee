package employeeinfo;

import java.time.LocalDate;

public class Employee implements Comparable<Employee>{

	private AccountList accountList;

	private String name;
	private LocalDate hireDate;
	
	public Employee(String name,int yearOfHire, int monthOfHire, int dayOfHire){
		this.name = name;
		hireDate = LocalDate.of(yearOfHire,monthOfHire,dayOfHire);
		accountList = new AccountList();
	}

	
	public void createNewChecking(double startAmount) {
		Account acct = new CheckingAccount(this, startAmount);
		accountList.add(acct);
	}

	public void createNewSavings(double startAmount) {
		Account acct = new SavingsAccount(this, startAmount);
		accountList.add(acct);
	}

	public void createNewRetirement(double startAmount) {
		Account acct = new RetirementAccount(this, startAmount);
		accountList.add(acct);
	}

	public String getFormattedAcctInfo() {
		String output = "";
		for (int i = 0; i < accountList.size(); i++) {
			output += accountList.get(i).toString();
		}
		return "ACCOUNT INFO FOR " + this.getName() + "\n\n" + output + "\n\n";
	}

	public MyArrayList getAccounts(){
		MyArrayList names = new MyArrayList();
		for(int i = 0; i < accountList.size(); ++i){
			names.add(accountList.get(i).getAcctType().toString().toLowerCase());
		}
		return names;

	}

	public void deposit(int accId, double amt){
		Account selected = accountList.get(accId);
		selected.makeDeposit(amt);
	}
	public boolean withdraw(int accId, double amt){
		Account selected = accountList.get(accId);
		return selected.makeWithdrawal(amt);
	}

	private String format = "name = $s, salary = %.2f, hireDay = %s";

	@Override
	public String toString() {
		return this.getName() + " : " + this.getMax();
	}

	public String getName() {
		return name;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}

	public double getMax() {
		double max = accountList.get(0).getBalance();

		for (int i = 1; i < accountList.size(); i++) {
			if (max < accountList.get(i).getBalance())
				max = accountList.get(i).getBalance();
		}
		return max;
	}

	@Override
	public int compareTo(Employee o) {
		if (this.getMax() > o.getMax())
			return 1;
		else if (this.getMax() < o.getMax())
			return -1;
		else
			return 0;
	}
}

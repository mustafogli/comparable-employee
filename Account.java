package employeeinfo;

abstract public class Account {
    public static final AccountType CHECKING = AccountType.CHECKING;
    public static final AccountType SAVINGS = AccountType.SAVINGS;
    public static final AccountType RETIREMENT = AccountType.RETIREMENT;

    private final static double DEFAULT_BALANCE = 0.0;
    private final static double MAX_BALANCE = 1000;
    private double balance;
    private Employee employee;

    Account(Employee emp, double balance) {
        employee = emp;
        this.balance = balance;
    }

    Account(Employee emp) {
        this(emp, DEFAULT_BALANCE);
    }

    public String toString() {
        String result = "Account type: "+getAcctType().toString().toLowerCase()+ "\nCurrent bal:  "+balance +"\n";
        return result;
    }

    public void makeDeposit(double deposit) {
        this.balance += deposit;
    }

    public boolean makeWithdrawal(double amount) {
        if(amount>MAX_BALANCE){
            return false;
        }else{
            this.balance -= amount;
            return true;
        }
    }

    public void updateBalance(){

    }

    abstract public AccountType getAcctType();

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}

class CheckingAccount extends Account{
    private final double MONTHLY_SERVICE_CHARGE = 5.00;

    public CheckingAccount(Employee emp, double balance) {
        super(emp, balance);
    }

    public AccountType getAcctType() {
        return Account.CHECKING;
    }

    public CheckingAccount(Employee e){
        super(e);
    }

    @Override
    public void updateBalance() {
        double baseBalance = super.getBalance();
        setBalance(baseBalance - MONTHLY_SERVICE_CHARGE);
    }
}

class SavingsAccount extends Account{
    private final double MONTHLY_INTEREST_RATE = 0.25;

    public SavingsAccount(Employee emp, double balance) {
        super(emp, balance);
    }

    public SavingsAccount(Employee e){
        super(e);
    }

    public AccountType getAcctType() {
        return Account.SAVINGS;
    }

    @Override
    public void updateBalance() {
        double baseBalance = super.getBalance();
        double interest = (MONTHLY_INTEREST_RATE/100) * baseBalance;
        setBalance(baseBalance + interest);
    }
}

class RetirementAccount extends Account{
    private final double PENALTY_PERCENT = 2.0;

    public RetirementAccount(Employee emp, double balance) {
        super(emp, balance);
    }

    public AccountType getAcctType() {
        return Account.RETIREMENT;
    }

    @Override
    public boolean makeWithdrawal(double amount) {
        double balance = super.getBalance();

        double penalty = balance * (PENALTY_PERCENT/100);
        balance = balance - amount - penalty;
        if(balance < 0){
            return false;
        }
        setBalance(balance);
        return true;
    }
}
package employeeinfo;

import java.util.Arrays;

public class MyArrayList {
    private final int INITIAL_LENGTH = 2;
    private String[] strArray;
    private int size;

    public MyArrayList() {
        strArray = new String[INITIAL_LENGTH];
        size = 0;
    }

    public void add(String s){
        if(s==null){
            return;
        }
        if(size==strArray.length) {
            this.resize();
        }
        strArray[size++] = s;
    }

    public String get(int i){
        return strArray[i];
    }

    public boolean find(String s){
        for (String value : strArray) {
            if (s.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public void insert(String s, int pos){
        if(pos> strArray.length || pos<0)
            return;

        this.resize(); // N => N+1
        size++;
        for(int i=size-1;i>pos;i--)
        {
            strArray[i]=strArray[i-1];
        }
        strArray[pos]=s;
    }

    public boolean remove(String s){
        if(s==null)
            return false;
        for(int i=0;i<size;i++) {
            if (s.equals(strArray[i])) {
                for(int j=i;j<size-1;j++){
                    strArray[j] = strArray[j + 1];
                }
                size--;
                return true;
            }
        }
        return false;

    }


    private void resize(){
        strArray= Arrays.copyOf(strArray,strArray.length+size);
    }
    public String toString(){
        String str="[";
        for(int i=0;i<size;i++) {
            str+=strArray[i];
            if(i<size-1)
                str+=",";
        }

        str+="]";
        return str;
    }
    public int size() {
        return this.size;
    }
}
